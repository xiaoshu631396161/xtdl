/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - xtdl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xtdl` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `xtdl`;

/*Table structure for table `xel_fhyc` */

DROP TABLE IF EXISTS `xel_fhyc`;

CREATE TABLE `xel_fhyc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(255) DEFAULT NULL COMMENT '地区',
  `date` varchar(50) DEFAULT NULL COMMENT '时间',
  `mean_C` varchar(50) DEFAULT NULL COMMENT '平均温度',
  `top_C` varchar(50) DEFAULT NULL COMMENT '最高温',
  `bottom_C` varchar(50) DEFAULT NULL COMMENT '最低温',
  `precipitation` varchar(50) DEFAULT NULL COMMENT '降水量（毫米）',
  `humidity` varchar(50) DEFAULT NULL COMMENT '平均湿度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `xel_fhyc` */

insert  into `xel_fhyc`(`id`,`area`,`date`,`mean_C`,`top_C`,`bottom_C`,`precipitation`,`humidity`) values (1,'华东','2018-05-10','25','20','30','33','70'),(2,'华西','2018-05-10','25','20','30','33','70'),(3,'华南','2018-05-10','25','20','30','33','70'),(4,'华北','2018-05-10','25','20','30','33','70'),(5,'华东','2018-05-11','24','19','29','31','77');

/*Table structure for table `xel_hygl` */

DROP TABLE IF EXISTS `xel_hygl`;

CREATE TABLE `xel_hygl` (
  `contract_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '合约ID',
  `contract_name` varchar(255) DEFAULT NULL COMMENT '合约名称',
  `user_name` varchar(255) DEFAULT NULL COMMENT '客户名称',
  `start_time` varchar(255) DEFAULT NULL COMMENT '开始时间',
  `over_time` varchar(255) DEFAULT NULL COMMENT '结束时间',
  `contract_electricity` varchar(255) DEFAULT NULL COMMENT '合约电量',
  `long_covariance` varchar(255) DEFAULT NULL COMMENT '长协电量',
  `bidding_energy` varchar(255) DEFAULT NULL COMMENT '竞价电量',
  `appendix` varchar(255) DEFAULT NULL COMMENT '附件',
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `xel_hygl` */

insert  into `xel_hygl`(`contract_id`,`contract_name`,`user_name`,`start_time`,`over_time`,`contract_electricity`,`long_covariance`,`bidding_energy`,`appendix`) values (1,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(2,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(3,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(4,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(5,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(6,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(7,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(8,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(9,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(10,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(11,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(12,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(13,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(14,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(15,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(16,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(17,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(18,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(19,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(20,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(21,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(22,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(23,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(24,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无'),(25,'漳州芗城区','甲骨文','2018-05-10','2018-06-10','100','100','100','无');

/*Table structure for table `xel_jsg` */

DROP TABLE IF EXISTS `xel_jsg`;

CREATE TABLE `xel_jsg` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `buyname` varchar(255) DEFAULT NULL,
  `sellname` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `buydian` varchar(255) DEFAULT NULL,
  `selldian` varchar(255) DEFAULT NULL,
  `buyjiage` varchar(255) DEFAULT NULL,
  `create_time` int(250) DEFAULT NULL,
  `update_time` int(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `xel_jsg` */

insert  into `xel_jsg`(`id`,`buyname`,`sellname`,`date`,`buydian`,`selldian`,`buyjiage`,`create_time`,`update_time`) values (11,'dsas','dsad','01-05-2018','111','111','111',1526029695,1526029695),(12,'萨达','萨达啊实打实','11-05-2018','500','115','3213',1526544524,1526544524);

/*Table structure for table `xel_jygl` */

DROP TABLE IF EXISTS `xel_jygl`;

CREATE TABLE `xel_jygl` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `contract_id` int(255) DEFAULT NULL,
  `contract_name` varchar(255) DEFAULT NULL,
  `contract_type` varchar(255) DEFAULT NULL,
  `contract_electricity` varchar(255) DEFAULT NULL,
  `contract_year` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `xel_jygl` */

insert  into `xel_jygl`(`id`,`contract_id`,`contract_name`,`contract_type`,`contract_electricity`,`contract_year`) values (1,1,'漳州芗城区','长期','100','10'),(2,2,'漳州芗城区','长期','100','10'),(3,3,'漳州芗城区','长期','100','10'),(4,4,'漳州芗城区','长期','100','10'),(5,5,'漳州芗城区','长期','100','10'),(6,6,'漳州芗城区','长期','100','10'),(7,7,'漳州芗城区','长期','100','10'),(8,8,'漳州芗城区','长期','100','10'),(9,9,'漳州芗城区','长期','100','10'),(10,10,'漳州芗城区','长期','100','10'),(11,11,'漳州芗城区','长期','100','10'),(12,12,'漳州芗城区','长期','100','10'),(13,13,'漳州芗城区','长期','100','10'),(14,14,'漳州芗城区','长期','100','10'),(15,15,'漳州芗城区','长期','100','10'),(16,16,'漳州芗城区','长期','100','10'),(17,17,'漳州芗城区','长期','100','10'),(18,18,'漳州芗城区','长期','100','10'),(19,19,'漳州芗城区','长期','100','10'),(20,20,'漳州芗城区','长期','100','10'),(21,21,'漳州芗城区','长期','100','10'),(22,22,'漳州芗城区','长期','100','10'),(23,23,'漳州芗城区','长期','100','10'),(24,24,'漳州芗城区','长期','100','10'),(25,25,'漳州芗城区','长期','100','10');

/*Table structure for table `xel_load` */

DROP TABLE IF EXISTS `xel_load`;

CREATE TABLE `xel_load` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(255) DEFAULT NULL,
  `max_temperature` float DEFAULT NULL,
  `min_temperature` float DEFAULT NULL,
  `rainfull` varchar(255) DEFAULT NULL,
  `humidity` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `xel_load` */

insert  into `xel_load`(`id`,`area`,`max_temperature`,`min_temperature`,`rainfull`,`humidity`,`date`) values (1,'中南',43,23,'44','33','2018-05-17 10:25:03'),(7,'as',0,0,'','','2018-05-17 16:41:26'),(8,'',0,0,'','','2018-05-17 17:06:02');

/*Table structure for table `xel_map` */

DROP TABLE IF EXISTS `xel_map`;

CREATE TABLE `xel_map` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(15) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/*Data for the table `xel_map` */

insert  into `xel_map`(`id`,`province`,`value`) values (1,'北京','500'),(2,'天津','600'),(3,'上海','700'),(4,'重庆','800'),(5,'河北','900'),(6,'山西','1000'),(7,'辽宁','1100'),(8,'吉林','1200'),(9,'黑龙江','1300'),(10,'江苏','1400'),(11,'浙江','1500'),(12,'安徽','1600'),(13,'福建','1700'),(14,'江西','1800'),(15,'山东','1900'),(16,'河南','2000'),(17,'湖北','2100'),(18,'湖南','2200'),(19,'广东','2300'),(20,'海南','2400'),(21,'四川','2500'),(22,'贵州','2600'),(23,'云南','2700'),(24,'陕西','2800'),(25,'甘肃','2900'),(26,'青海','3000'),(27,'台湾','3100'),(28,'内蒙古','3200'),(29,'自治区','3300'),(30,'广西','3400'),(31,'西藏','3500'),(32,'宁夏','3600'),(33,'新疆','3700'),(34,'香港','3800'),(35,'澳门','3900');

/*Table structure for table `xel_sdgl` */

DROP TABLE IF EXISTS `xel_sdgl`;

CREATE TABLE `xel_sdgl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(25) DEFAULT NULL,
  `data` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `xel_sdgl` */

insert  into `xel_sdgl`(`id`,`time`,`data`) values (1,'00:00','25'),(2,'02:00','40'),(3,'04:00','60'),(4,'06:00','100'),(5,'08:00','150'),(6,'10:00','190'),(7,'12:00','170'),(8,'14:00','140'),(9,'16:00','110'),(10,'18:00','60'),(11,'20:00','10');

/*Table structure for table `xel_sdgl2` */

DROP TABLE IF EXISTS `xel_sdgl2`;

CREATE TABLE `xel_sdgl2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` varchar(25) DEFAULT NULL,
  `data` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `xel_sdgl2` */

insert  into `xel_sdgl2`(`id`,`time`,`data`) values (1,'00:00','25'),(2,'02:00','40'),(3,'04:00','60'),(4,'06:00','100'),(5,'08:00','150'),(6,'10:00','190'),(7,'12:00','170'),(8,'14:00','140'),(9,'16:00','110'),(10,'18:00','300'),(11,'20:00','10');

/*Table structure for table `xel_shuju` */

DROP TABLE IF EXISTS `xel_shuju`;

CREATE TABLE `xel_shuju` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sj1` int(11) DEFAULT NULL,
  `bfb1` float DEFAULT NULL,
  `sj2` int(11) DEFAULT NULL,
  `bfb2` float DEFAULT NULL,
  `sj3` int(11) DEFAULT NULL,
  `bfb3` float DEFAULT NULL,
  `sj4` int(11) DEFAULT NULL,
  `bfb4` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `xel_shuju` */

insert  into `xel_shuju`(`id`,`sj1`,`bfb1`,`sj2`,`bfb2`,`sj3`,`bfb3`,`sj4`,`bfb4`) values (1,253,20.5,77,70.123,152,100.33,67,52);

/*Table structure for table `xel_total` */

DROP TABLE IF EXISTS `xel_total`;

CREATE TABLE `xel_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(255) DEFAULT NULL,
  `hour0` varchar(50) DEFAULT NULL,
  `hour2` varchar(50) DEFAULT NULL,
  `hour4` varchar(50) DEFAULT NULL,
  `hour6` varchar(50) DEFAULT NULL,
  `hour8` varchar(50) DEFAULT NULL,
  `hour10` varchar(50) DEFAULT NULL,
  `hour12` varchar(50) DEFAULT NULL,
  `hour14` varchar(50) DEFAULT NULL,
  `hour16` varchar(50) DEFAULT NULL,
  `hour18` varchar(50) DEFAULT NULL,
  `hour20` varchar(50) DEFAULT NULL,
  `hour22` varchar(50) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `xel_total` */

insert  into `xel_total`(`id`,`data`,`hour0`,`hour2`,`hour4`,`hour6`,`hour8`,`hour10`,`hour12`,`hour14`,`hour16`,`hour18`,`hour20`,`hour22`,`type`) values (1,'2018-05-10','0','20','40','60','80','100','120','140','160','180','200','220',1),(2,'','123','54','156','81','268','12','84','213','21','541','854','482',2),(3,'2018-05-11','0','10','30','50','70','90','110','130','150','170','190','210',1),(4,'','554','13','854','132','145','555','555','111','13','54','123','15',3),(5,NULL,'40','85','123','45','12','64','148','123','84','35','48','635',4),(6,NULL,'50','64','46','84','84','16','484','654','56','64','412','84',5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
