<?php
/**
 * Created by PhpStorm.
 * User: 邓彪
 * Date: 2018/5/11
 * Time: 16:07
 */



namespace app\home\controller;


use think\Controller;
use think\Model;
class Load extends Controller
{
    function fhyc(){

        //$result=Db::table('xel_load')->where('id',1)->find();
        // $result=db("xel_load")->select();
//  var_dump($result);
        // 渲染模板输出
        //$this->redirect('Load/admin-fhyc', ['result'=>$result]);
//    $this->redirect('');

//    view->result = $result;
//    return $view->fetch("admin-fhyc");
        //return $this->fetch("admin-fhyc");
           //   return view("admin-fhyc",$result);
        $result=\app\home\model\Load::paginate(5);
        $this-> assign('result',$result);
        return view();
    }
    function select(){
        $arr=$_POST;
        $date1=$arr['date1'];
        $date2=$arr['date2'];
        $area=$arr['area1'];
//        User::where('id','>',10)->select();
        $result=db('load')
            ->where('date','>',$date1)
            ->where('date','<',$date2)
            ->where('area',$area)
            ->select();
        echo json_encode($result);
    }

    function find(){
        $arr=$_POST;
        $id=$arr['id'];
        $result=db('load')->where('id',$id)->find();

        echo json_encode($result);
    }
    //修改的后台接口
    function update(){
        $arr=$_REQUEST;

        $id=$arr['id'];
        $qy=$arr['up_qy'];
        $lqw=$arr['up_lqw'];
        $sqw=$arr['up_sqw'];
        $yl=$arr['up_yl'];
        $sd=$arr['up_sd'];
        $result=db('load')
            ->where('id',$id)
            ->update([
                'area'=>$qy,
                'max_temperature'=>$lqw,
                'min_temperature'=>$sqw,
                'rainfull'=>$yl,
                'humidity'=>$sd,
            ]);
        if($result==1){
            $this->success('修改成功', 'Load/fhyc');
        }
    }

    function insert(){
        $arr=$_REQUEST;
        $qy=$arr['area'];
        $lqw=$arr['insert_lqw'];
        $sqw=$arr['insert_sqw'];
        $yl=$arr['insert_yl'];
        $sd=$arr['insert_sd'];

        $data=[
            'area'=>$qy,
            'max_temperature'=>$lqw,
            'min_temperature'=>$sqw,
            'rainfull'=>$yl,
            'humidity'=>$sd
        ];
        //db('表明')：这个属于助手函数
        $result=db('load')->insert($data);
        if($result==1){
            $this->success('Load/fhyc');
        }

    }
//删除接口
    function delete(){
        $id=$_POST['arr'];
        $load=\app\home\model\Load::destroy($id);
        // $result=db('load')->where('id',$id[0])->delete();     助手函数不支持数组删除


        if($load>=1){
            echo json_encode("删除成功！");
        }
    }
}