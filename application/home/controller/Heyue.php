<?php
/**
 * Created by PhpStorm.
 * User: gljmy
 * Date: 2018/5/14
 * Time: 9:22
 */
namespace app\home\controller;
use think\Controller;
use think\View;
use think\Db;
class Heyue extends Controller
{
    public  function  heyue(){

        // 查询状态为1的用户数据 并且每页显示10条数据
        $list = Db::name('hygl')->order("contract_id asc")->paginate(10);
        $demo = count($list);
        // 把分页数据赋值给模板变量list
        $this->assign('list', $list);
        // 渲染模板输出
        $page = $list->render();
        $this->assign('page', $page);
        $this->assign('demo',$demo);


        return view('admin-hygl');
    }


}