<?php
/**
 * Created by PhpStorm.
 * User: Timor~Home
 * Date: 2018/5/11
 * Time: 9:49
 */

namespace app\home\controller;
use think\Controller;
use think\Db;
use think\View;

class Kehu extends Controller
{
	
	
    public  function  kehu(){
    	$datas=Db::query('select * from `xel_map` ');
		$provinces=array();
		$values=array();
		foreach ($datas as $key => $value) {
			array_push($provinces,$value["province"]);
			array_push($values,$value["value"]);
		}
		
		$this->assign("provinces",$provinces);
		$this->assign("values",$values);

        //所有页面的相同的代码

        return view('admin-kehu');
    }

}