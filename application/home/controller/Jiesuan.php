<?php
/**
 * Created by PhpStorm.
 * Author: shishiwenjie
 * Date: 2018/5/11
 * Time: 9:22
 */

namespace app\home\controller;
use think\Controller;
use think\View;
class Jiesuan extends Controller
{
   public  function  jiesuan(){
       $arr=array();
       $arr['code'] ="";
       $arr['message'] = "";
       $this->assign("arr",$arr);



       return view('admin-jsgl');
   }
    function  sub(){
        $arr = array();
        $buyname=$_POST['buyname'];
        $sellname=$_POST['sellname'];
        $date=$_POST['date'];
        $buydian=$_POST['buydian'];
        $selldian=$_POST['selldian'];
        $buyjiage=$_POST['buyjiage'];
        if(empty($buyname))$this->error("请填写购方名称");
        if(empty($sellname))$this->error("请填写售方名称");
        if(empty($date))$this->error("请选择结算年月");
        if(empty($buydian))
        {
            $this->error("请填写购方电量");
        }elseif(!preg_match("/^\d*$/",$buydian)){
            $this->error("请在购方电量一栏中填写数字");
        }
        if(empty($selldian))
        {
            $this->error("请填写售方电量");
        }elseif(!preg_match("/^\d*$/",$selldian)){
            $this->error("请在售方电量一栏中填写数字");
        }
        if(empty($buyjiage))
        {
            $this->error("请填写购方电价");
        }elseif(!preg_match("/^\d*$/",$buyjiage)){
            $this->error("请在购方电价一栏中填写数字");
        }
        $data=[
            'buyname'=>$buyname,
            'sellname'=>$sellname,
            'date'=>$date,
            'buydian'=>$buydian,
            'selldian'=>$selldian,
            'buyjiage'=>$buyjiage
        ];
        $add=new \app\home\model\jiesuan();
        $res=$add->save($data);
        if($res){
            $this->success("添加成功！3s后返回界面","jiesuan");
        }else{
            $this->error("添加失败！","jiesuan");
        }
    }
}